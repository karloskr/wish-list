import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje/destino-viaje.model';
import { AppState } from 'src/app/app.module';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from 'src/app/models/destino-viaje/destino-viajes-state-model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @Input('idx') position: number;
  @HostBinding('attr.class') ccsClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(  private store: Store<AppState> ) {
    //this.nombre = 'Nombre';
    this.clicked = new EventEmitter();
    
   }

  ngOnInit() {
  }
/* Vamos a emitr un evento cuando se le de click al boton ir */
  ir() {
    this.clicked.emit( this.destino );
    /* Para que la pagina no se actualice*/
    return false;
  }

  voteUp() {
    this.store.dispatch( new VoteUpAction( this.destino ));
    return false;
  }

  voteDown() {
    this.store.dispatch( new VoteDownAction( this.destino ));
    return false;
  }
}
