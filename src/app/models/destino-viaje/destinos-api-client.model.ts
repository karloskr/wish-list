import { DestinoViaje } from './destino-viaje.model';
//import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from "@ngrx/store";
import { AppState, AppConfig, APP_CONFIG, db } from 'src/app/app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destino-viajes-state-model';
import { Injectable, forwardRef, Inject } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from "@angular/common/http";

@Injectable()
export class DestinosApiClient {
    destinos:DestinoViaje[];
    //crear objeto importar rxjs
    //current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
    /* El elegido comienza en null (Observable)*/
    constructor( private store: Store<AppState>,
      @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
      private http: HttpClient
       ) {
      this.store
      .select(state => state.destinos)
      .subscribe((data) => {
          console.log("destinos sub store");
          console.log(data);
          this.destinos = data.items;
      });
      this.store
      .subscribe((data) => {
          console.log("all store");
          console.log(data);
      });
    }
    
    add(d: DestinoViaje) {
      const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
      const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
      this.http.request(req).subscribe((data: HttpResponse<{}>) => {
        if (data.status === 200) {
          this.store.dispatch(new NuevoDestinoAction(d));
          const myDb = db;
          myDb.destinos.add(d);
          console.log('todos los destinos de la db!');
          myDb.destinos.toArray().then(destinos => console.log(destinos))
        }
      });
    }

      getAll(): DestinoViaje[]{
        return this.destinos;
      }

      getById( id:String ): DestinoViaje {
        return this.destinos.filter(function( d ) {return d.id.toString() === id; })[0];
      }

      elegir(d: DestinoViaje) {
        //invocar el servidor
        this.store.dispatch( new ElegidoFavoritoAction(d));
        
    }

    /*Se agrega el observable
    Se le indica a observable cual es actualmente el elegido, empieza null y se propaga el evento
    */
   /*
    elegir(destino: DestinoViaje) {
        this.destinos.forEach(x => x.setSelected(false));
        destino.setSelected(true);
        this.current.next(destino);
    }

    //Metodo para subscribir las actualizaciones
    subscribeOnChange(fn) {
        this.current.subscribe(fn);
    }
    */
}